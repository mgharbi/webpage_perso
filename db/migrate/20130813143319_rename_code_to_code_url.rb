class RenameCodeToCodeUrl < ActiveRecord::Migration
  def change
    rename_column :projects, :code, :code_url
  end
end
