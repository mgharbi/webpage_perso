class AddCodePdfBibToProject < ActiveRecord::Migration
  def change
    add_column :projects, :code, :string
    add_column :projects, :pdf, :string
    add_column :projects, :bib, :text
  end
end
