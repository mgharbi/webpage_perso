class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.text :abstract
      t.boolean :display

      t.timestamps
    end
  end
end
