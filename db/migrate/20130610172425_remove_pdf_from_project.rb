class RemovePdfFromProject < ActiveRecord::Migration
  def up
    remove_column :projects, :pdf
  end

  def down
    add_column :projects, :pdf, :string
  end
end
