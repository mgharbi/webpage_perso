class AddAttachmentPdfToProjects < ActiveRecord::Migration
  def self.up
    change_table :projects do |t|
      t.attachment :pdf
    end
  end

  def self.down
    drop_attached_file :projects, :pdf
  end
end
