require 'rufus/scheduler'
scheduler = Rufus::Scheduler.start_new
 
if Rails.env.production?
  scheduler.every '30m' do
    require "net/http"
    require "uri"
    url = 'http://www.google.com'
    Net::HTTP.get_response(URI.parse(ENV["HOSTNAME"]))
  end
end
