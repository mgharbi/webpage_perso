class Project < ActiveRecord::Base
  attr_accessible :name,
                  :author,
                  :abstract,
                  :bib,
                  :display,
                  :code_url,
                  :thumbnail,
                  :delete_thumbnail,
                  :pdf,
                  :delete_pdf,
                  :image,
                  :delete_image

  attr_accessor :delete_thumbnail
  attr_accessor :delete_pdf
  attr_accessor :delete_image

  # Clean-up attachment deletions
  before_validation { thumbnail.clear if delete_thumbnail == '1' }
  before_validation { pdf.clear if delete_pdf == '1' }
  before_validation { image.clear if delete_image == '1' }

  validates :abstract,  :presence => true
  validates :name,      :presence => true
  validates :author,    :presence => true
  validates :pdf, attachment_content_type: {content_type:"application/pdf"}
  validates :thumbnail, attachment_content_type: {content_type:["image/jpeg","image/png"]}
  validates :image, attachment_content_type: {content_type:["image/jpeg", "image/png"]}

  has_attached_file :pdf,
                    :url => "/assets/pdf/:pdf_name.:extension"
                    # :path => ":rails_root/assets/pdf/:pdf_name.:extension"
  has_attached_file :thumbnail,
                    :styles => { :thumb => "150x150>" },
                    :url => "/assets/images/projects/:thumb_name.:extension"
                    # :path => "/images/projects/:thumb_name.:extension"
  has_attached_file :image,
                    :styles => { :large => "870x" },
                    :url => "/assets/images/projects/:image_name.:extension"
                    # :path => "/images/projects/:image_name.:extension"

  def pdf_name
    if self.pdf_file_name
      "#{self.name.truncate(30,separator:' ',omission:'').downcase.gsub(" ", "_")}"
    else
      ""
    end
  end
  def thumb_name
    if self.thumbnail_file_name
      "thumb_#{self.name.truncate(30,separator:' ',omission:'').gsub(" ", "_")}"
    else
      ""
    end
  end
  def image_name
    if self.image_file_name
      "image_#{self.name.truncate(30,separator:' ',omission:'').gsub(" ", "_")}"
    else
      ""
    end
  end

  Paperclip.interpolates :pdf_name do |attachment, style|
    attachment.instance.pdf_name
  end
  Paperclip.interpolates :thumb_name do |attachment, style|
    attachment.instance.thumb_name
  end
  Paperclip.interpolates :image_name do |attachment, style|
    attachment.instance.image_name
  end
end

