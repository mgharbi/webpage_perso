class ProjectController < ApplicationController
  layout "project"

  caches_page :show, :show_bib
  cache_sweeper :project_sweeper

  def index
    redirect_to index_url
  end

  def show
    @project = Project.find(params[:id])
  end

  def show_bib
    @project = Project.find(params[:id])
    render :layout => "home"
  end
end
