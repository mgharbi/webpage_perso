class HomeController < ApplicationController
  layout "home"

  # caches_action :index, :cv, :bio, :misc
  caches_page :index, :cv, :bio, :misc
  cache_sweeper :project_sweeper

  def index
    @projects = Project.where(display:true)
    @project  = @projects.first
  end

  def cv
  end

  def bio
  end

  def misc
  end
end
