class ProjectSweeper < ActionController::Caching::Sweeper
  observe :project

  # If our sweeper detects that a project was created call this
  def after_create(project)
    expire_cache_for(project)
  end

  # If our sweeper detects that a project was updated call this
  def after_update(project)
    expire_cache_for(project)
  end

  # If our sweeper detects that a project was deleted call this
  def after_destroy(project)
    expire_cache_for(project)
  end

  private
  def expire_cache_for(project)
    expire_page(controller: '/home', action: 'index')
    expire_page(controller: '/project', action: 'show', id: project.id)
    expire_page(controller: '/project', action: 'show_bib', id: project.id)
    puts 'CACHE Expired for home/index, show, show_bib'
  end
end

