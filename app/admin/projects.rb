ActiveAdmin.register Project do
  config.sort_order = "name_asc"
  config.filters = false
  config.per_page   = 10

  controller do
    cache_sweeper :project_sweeper
  end

  index :download_links => false do
    selectable_column
    column :name
    column :author
    column :display
    default_actions
  end

  form :html => { :multipart => true } do |f| 
    f.inputs "Data" do 
      f.input :name
      f.input :author
      f.input :abstract
      f.input :bib, label: "BibTeX"
      f.input :code_url, label: "Code URL"
      f.input :display, label: "Publish online"
    end
    f.inputs "Attachments", multipart: true do
      f.input :pdf, label: "PDF", hint: f.object.pdf_name
      f.input :delete_pdf, label: "Delete PDF", as: :boolean
      f.input :thumbnail, hint: "#{f.object.thumb_name} (150px wide)"
      f.input :delete_thumbnail, as: :boolean
      f.input :image, hint: "#{f.object.image_name} (870px wide)"
      f.input :delete_image, as: :boolean
    end
    f.actions
  end

  show do |p|
    attributes_table do
      row :name
      row :author
      row :abstract
      row :created_at
      row :updated_at
      row :display
      row "PDF" do
        p.pdf_name
      end
      row "Thumbnail" do
        p.thumb_name
      end
      row "Image" do
        p.image_name
      end
      row :code_url do
        link_to p.code_url, p.code_url
      end

    end
  end
  
end
